import { ITransaction, ITransactionCount } from './transaction.interface';

export class TransactionsStateModel {
    data?: Array<ITransaction>;
    error?: object;
    total?: number;
    counts?: Array<ITransactionCount>;
    isLoading?: boolean;
}

export class Get {
    static readonly type = '[Transactions] Get';

    constructor() {}
}

export class GetByType {
    static readonly type = '[Transactions] GetByType';

    constructor(public tab: string) {}
}

export class GetCounts {
    static readonly type = '[Transactions] GetCounts';

    constructor() {}
}
