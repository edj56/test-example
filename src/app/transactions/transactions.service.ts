import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import ICollection from '../core/collection.interface';
import { ITransaction, ITransactionCount } from './transaction.interface';


@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  types: object = {
    0: 'income',
    1: 'outcome',
    2: 'loan',
    3: 'investment',
  };

  constructor(private http: HttpClient) { }

  get(): Observable<ICollection<ITransaction>> {
    return this.http.get<ICollection<ITransaction>>('assets/transactions.data.json');
  }

  getByType(tab: string): Observable<ICollection<ITransaction>> {
    return this.get().pipe(
      map(response => ({ data: response.data.filter(({ type }) => type === this.types[tab]) })),
      map(({ data }) => ({ total: data.length, data }))
    );
  }

  getCounts(): Observable<Array<ITransactionCount>> {
    return this.get().pipe(
      map(({ data }) => data),
      map(data => Object.entries(this.types).map(([tab, type]) =>
          [ tab, type, data.filter(el => el.type === type).length ]
        )
      ),
      map(counts => counts.map<ITransactionCount>(([tab, label, count]: any) => ({ label, count, tab })))
    );
  }
}
