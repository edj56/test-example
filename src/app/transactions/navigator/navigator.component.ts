import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

import { Get, GetByType, TransactionsStateModel } from '../transactions.actions';
import { TransactionsState } from '../transactions.state';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.scss']
})
export class NavigatorComponent implements OnInit, OnDestroy {
  @Select(TransactionsState) public transactions$: Observable<TransactionsStateModel>;

  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private store: Store,
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.queryParams.subscribe(({ tab }) => {
      if (!!tab) {
        return this.store.dispatch(new GetByType(tab));
      }

      return this.store.dispatch(new Get());
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
