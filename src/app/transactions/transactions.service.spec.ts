import { TestBed } from '@angular/core/testing';

import { TransactionsService } from './transactions.service';
import { HttpClientModule } from '@angular/common/http';

describe('TransactionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: TransactionsService = TestBed.get(TransactionsService);
    expect(service).toBeTruthy();
  });

  it('should get 100 items', (done: DoneFn) => {
    const service: TransactionsService = TestBed.get(TransactionsService);

    service.get().subscribe(response => {
      expect(response.total).toEqual(100);
      expect(response.data.length).toEqual(100);
      done();
    });
  });

  it('should get 18 items for tab 0', (done: DoneFn) => {
    const service: TransactionsService = TestBed.get(TransactionsService);

    service.getByType('0').subscribe(response => {
      expect(response.total).toEqual(18);
      expect(response.data.length).toEqual(18);
      done();
    });
  });

  it('should get array with counts, length default types 4', (done: DoneFn) => {
    const service: TransactionsService = TestBed.get(TransactionsService);

    const types = Object.values(service.types);

    service.getCounts().subscribe(response => {
      expect(response.length).toEqual(4);
      expect(response.map(el => el.label)).toEqual(types);
      done();
    });
  });

  it('should get array with counts with length 2 if types are custom', (done: DoneFn) => {
    const service: TransactionsService = TestBed.get(TransactionsService);

    service.types = { 0: 'some_type_which_does_not_exist_in_data ' };
    const types = Object.values(service.types);

    service.getCounts().subscribe(response => {
      expect(response.length).toEqual(1);
      expect(response.map(el => el.label)).toEqual(types);
      done();
    });
  });

  it('should get number in counts', (done: DoneFn) => {
    const service: TransactionsService = TestBed.get(TransactionsService);

    service.getCounts().subscribe(response => {
      expect(typeof response[0].count).toEqual('number');
      done();
    });
  });
});
