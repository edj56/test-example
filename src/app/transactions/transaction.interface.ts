export interface ITransaction {
    _id: string;
    type: string;
    amount?: string;
    name?: object;
    company?: string;
    email?: string;
    phone?: string;
    address?: string;
}

export interface ITransactionCount {
    label: string;
    count: number;
    tab: number;
}
