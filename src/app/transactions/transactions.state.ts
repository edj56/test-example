import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { TransactionsStateModel, Get, GetByType, GetCounts } from './transactions.actions';
import { TransactionsService } from './transactions.service';


@State<TransactionsStateModel>({
  name: 'transactions',
  defaults: {
    data: [],
    error: {},
    total: 0,
    counts: [],
    isLoading: false
  }
})
@Injectable({
  providedIn: 'root'
})
export class TransactionsState {
  constructor(private transactionsService: TransactionsService) {}

  @Action(Get)
  get({ patchState }: StateContext<TransactionsStateModel>) {
    patchState({ isLoading: true });

    return this.transactionsService.get().pipe(tap(
      ({ data, total }) => patchState({ data, total }),
      ({ error }) => patchState({ error }),
      () => patchState({ isLoading: false })
    ));
  }

  @Action(GetByType)
  getByType({ patchState }: StateContext<TransactionsStateModel>, { tab }: GetByType) {
    patchState({ isLoading: true });

    return this.transactionsService.getByType(tab).pipe(tap(
      ({ data, total }) => patchState({ data, total }),
      ({ error }) => patchState({ error }),
      () => patchState({ isLoading: false })
    ));
  }

  @Action(GetCounts)
  getCounts({ patchState }: StateContext<TransactionsStateModel>) {
    patchState({ isLoading: true });

    return this.transactionsService.getCounts().pipe(tap(
      (counts) => patchState({ counts }),
      ({ error }) => patchState({ error }),
      () => patchState({ isLoading: false })
    ));
  }
}
