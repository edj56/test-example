import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule, Store } from '@ngxs/store';

import { SummaryComponent } from './summary.component';
import { SumPipe } from 'src/app/core/pipes/sum.pipe';
import { TransactionsState } from '../transactions.state';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { TransactionsService } from '../transactions.service';
import { of } from 'rxjs';
import { TransactionsStateModel } from '../transactions.actions';


describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryComponent, SumPipe ],
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
        NgxsModule.forRoot([TransactionsState]),
      ],
      providers: [
        TransactionsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
