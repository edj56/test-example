import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';

import { GetCounts, TransactionsStateModel } from '../transactions.actions';
import { Observable } from 'rxjs';
import { ITransactionCount } from '../transaction.interface';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  @Select() transactions$: Observable<TransactionsStateModel>;

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new GetCounts());
  }
}
