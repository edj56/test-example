import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { HttpClientModule } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SumPipe } from '../core/pipes/sum.pipe';

import { ClientsRoutingModule } from './transactions-routing.module';
import { TransactionsService } from './transactions.service';
import { TransactionsState } from './transactions.state';

import { TransactionsComponent } from './transactions.component';
import { NavigatorComponent } from './navigator/navigator.component';
import { SummaryComponent } from './summary/summary.component';


@NgModule({
  declarations: [
    TransactionsComponent,
    NavigatorComponent,
    SummaryComponent,
    SumPipe,
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    HttpClientModule,
    NgxsModule.forRoot([TransactionsState],
      { developmentMode: !environment.production }
    )
  ],
  providers: [TransactionsService]
})
export class TransactionsModule { }
