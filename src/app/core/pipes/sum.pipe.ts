import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sum'
})
export class SumPipe implements PipeTransform {

  transform(items: any[], key: string): number {
    return items.reduce((sum, item) => sum + item[key], 0);
  }

}
