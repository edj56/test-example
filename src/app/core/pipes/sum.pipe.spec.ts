import { SumPipe } from './sum.pipe';

describe('SumPipe', () => {
  let pipe;

  beforeEach(() => {
    pipe = new SumPipe();
  });


  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should sum', () => {
    const sum = pipe.transform([{ count: 1 }, { count: 2 }, { count: 3 }], 'count');

    expect(sum).toBe(6);
  });

  it('should return 0 if empty array', () => {
    const sum = pipe.transform([], 'count');

    expect(sum).toBe(0);
  });
});
