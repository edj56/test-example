export default interface ICollection<T> {
    total?: number;
    data: Array<T>;
}
